module.exports = {
  outputDir: '../frontend-server/web',
  assetsDir: 'dist',
  runtimeCompiler: true,
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      // https://github.com/oliverfindl/vue-svg-inline-loader
      .use('vue-svg-inline-loader')
      .loader('vue-svg-inline-loader')
      .options({
        svgo: {
          plugins: [
            {
              removeAttrs: {
                attrs: '*:(stroke|fill):((?!^none$).)*',
              },
            },
          ],
        },
      });
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options.transformAssetUrls = {
          img: 'src',
          image: 'xlink:href',
          'b-avatar': 'src',
          'b-img': 'src',
          'b-img-lazy': ['src', 'blank-src'],
          'b-card': 'img-src',
          'b-card-img': 'src',
          'b-card-img-lazy': ['src', 'blank-src'],
          'b-carousel-slide': 'img-src',
          'b-embed': 'src'
        }

        return options
      });
  },
};
