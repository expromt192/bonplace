module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-shadow': 'off',
    'no-param-reassign': 'off',
    'max-len': 'off',
    'no-unused-vars': 'off',
    'dot-notation': 'off',
    'object-curly-newline': 'off',
    'import/prefer-default-export': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint',
  }
};
