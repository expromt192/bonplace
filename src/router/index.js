import Vue from 'vue';
import VueRouter from 'vue-router';
import LayoutPartner from '../layouts/Partner.vue';
import LayoutClient from '../layouts/Client.vue';
import LayoutPartnerAuth from '../layouts/PartnerAuth.vue';
import Rules from '../views/Rules.vue';
import Policies from '../views/Policies.vue';
import PartnerDesktop from '../views/PartnerDesktop.vue';
import PartnerBilling from '../views/PartnerBilling.vue';
import PartnerInvoices from '../views/PartnerInvoices.vue';
import PartnerInvoiceDetails from '../views/PartnerInvoiceDetails.vue';
import PartnerProfile from '../views/PartnerProfile.vue';
import ChangePasswordModal from '../views/ChangePasswordModal.vue';
import store from '../store';
import i18n from '../i18n';
import PartnerLogin from '../views/auth/PartnerLogin.vue';
import PartnerPasswordReset from '../views/auth/PartnerPasswordReset.vue';
import PartnerNewPassword from '../views/auth/PartnerNewPassword.vue';
import UiKit from '../views/UiKit.vue';
import LandingPlaceholder from '../views/LandingPlaceholder.vue';
import LandingOffer from '../views/LandingOffer.vue';
import Catalog from '../views/Catalog.vue';
import Contacts from '../views/Contacts.vue';
import About from '../views/About.vue';
import Work from '../views/Work.vue';
import Bonus from '../views/Bonus.vue';
import Faq from '../views/Faq.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: LandingPlaceholder,
  },
  {
    path: '/ui-kit',
    component: UiKit,
  },
  {
    path: '/offer',
    component: LandingOffer,
  },
  {
    path: '/partner',
    component: LayoutPartner,
    children: [
      {
        path: '',
        redirect: 'desktop',
      },
      {
        path: 'desktop',
        component: PartnerDesktop,
      },
      {
        path: 'billing',
        component: PartnerBilling,
        children: [
          {
            path: '',
            redirect: 'invoice',
          },
          {
            name: 'partner-invoice-list',
            path: 'invoice',
            component: PartnerInvoices,
          },
          {
            name: 'partner-invoice-details',
            path: 'invoice/:id',
            component: PartnerInvoiceDetails,
          },
        ],
      },
      {
        path: 'profile',
        component: PartnerProfile,
        children: [
          {
            path: 'change-password',
            component: ChangePasswordModal,
            props: {
              backPath: '/partner/profile',
            },
          },
        ],
      },
    ],
    meta: { requiresAuth: true },
  },
  {
    path: '/partner',
    component: LayoutPartnerAuth,
    children: [
      {
        path: 'auth/login',
        component: PartnerLogin,
      },
      {
        path: 'auth/new-password/:resetToken',
        component: PartnerNewPassword,
      },
      {
        path: 'auth/password-reset',
        component: PartnerPasswordReset,
      },
    ],
    meta: { requiresNotAuth: true },
  },
  {
    path: '/partner',
    component: LayoutPartner,
    children: [
      {
        path: 'rules',
        component: Rules,
      },
    ],
  },
  {
    path: '/login-by-pass/:token',
    async beforeEnter(to, from, next) {
      const { success, errors } = await store.dispatch('loginByPass', to.params);
      if (success) {
        store.dispatch('showToast', {
          content: i18n.t('msg_logged_in'),
          variant: 'success',
          icon: 'icon-ok',
        });
      } else {
        store.dispatch('showToast', {
          content: errors.token,
          variant: 'danger',
          icon: 'icon-danger',
        });
      }
      next('/partner');
    },
  },
  {
    path: '/policies',
    component: LayoutClient,
    children: [
      {
        path: '',
        redirect: 'refund',
      },
      {
        path: ':slug',
        name: 'policy',
        component: Policies,
      },
    ],
  },
  {
    path: '/catalog',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: Catalog,
      },
    ],
  },
  {
    path: '/contacts',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: Contacts,
      },
    ],
  },
  {
    path: '/about',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: About,
      },
    ],
  },
  {
    path: '/work',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: Work,
      },
    ],
  },
  {
    path: '/bonus',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: Bonus,
      },
    ],
  },
  {
    path: '/faq',
    component: LayoutClient,
    children: [
      {
        path: '',
        component: Faq,
      },
    ],
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.isAuthenticated) {
    next({
      path: '/partner/auth/login',
      query: { redirect: to.fullPath },
    });
  } else if (to.matched.some(record => record.meta.requiresNotAuth) && store.getters.isAuthenticated) {
    next({
      path: '/partner',
    });
  } else {
    next();
  }
});

export default router;
