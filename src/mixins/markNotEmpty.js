export default {
  mounted() {
    const markNotEmpty = (element) => {
      if (element.value) {
        element.classList.add('not-empty');
      } else {
        element.classList.remove('not-empty');
      }
    };
    const targets = this.$el.querySelectorAll('.form-control-material');
    targets.forEach((target) => {
      markNotEmpty(target);
      target.addEventListener('input', (event) => {
        markNotEmpty(event.target);
      });
    });
  },
};
