export default {
  methods: {
    mixin_autoResize_resize(event) {
      const offset = event.target.offsetHeight - event.target.clientHeight;
      event.target.style.height = 'auto';
      event.target.style.height = `${event.target.scrollHeight + offset}px`;
    },
  },
  mounted() {
    this.$nextTick(() => {
      this.$el.setAttribute('style', 'height',
        `${this.$el.scrollHeight}px`);
    });
  },
};
