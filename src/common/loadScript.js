export default function loadScript(src, async = true) {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.onload = () => {
      resolve();
    };
    script.async = async;
    script.src = src;
    document.head.appendChild(script);
  });
}
