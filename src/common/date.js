function getDate(d) {
  if (d instanceof Date) {
    return d;
  }
  // eslint-disable-next-line no-restricted-globals
  if (!isNaN(d)) {
    d *= 1000; // convert to milliseconds
  }
  return new Date(d);
}

export function formatDate(d, locale) {
  const formatter = new Intl.DateTimeFormat(locale, { month: 'long', day: 'numeric', year: 'numeric' });
  return formatter.format(getDate(d));
}

export function formatDayMonth(d, locale) {
  const formatter = new Intl.DateTimeFormat(locale, { month: 'long', day: 'numeric' });
  return formatter.format(getDate(d));
}

export function formatYear(d, locale) {
  const formatter = new Intl.DateTimeFormat(locale, { year: 'numeric' });
  return formatter.format(getDate(d));
}

export function formatTime(d, locale) {
  const formatter = new Intl.DateTimeFormat(locale, { timeStyle: 'short' });
  return formatter.format(getDate(d));
}
