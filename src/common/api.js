export const api = new Map([
  ['login', '/auth/login'],
  ['loginByPass', '/auth/login-by-pass'],
  ['passwordReset', '/auth/recover'],
  ['setNewPassword', '/auth/new-password'],
  ['profile', '/profile'],
  ['user', '/user'],
  ['partner', '/partner'],
  ['partnerInvoice', '/partner-invoice'],
  ['partnerInvoiceStats', '/partner-invoice/stats'],
  ['partnerOperation', '/partner-operation'],
  ['partnerOperationStats', '/partner-operation/stats'],
  ['contact', '/contact'],
  ['page', '/page'],
  ['config', '/config'],
]);

export function getApiUrl(endpointName, id) {
  return (api.has(endpointName) ? api.get(endpointName) : '') + (id ? `/${id}` : '');
}
