import { required, sameAs, minLength, and } from 'vuelidate/lib/validators';

export const password = and(
  minLength(6),
  value => /\d/.test(value),
  value => /[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/.test(value),
);

export function fileSize(size) {
  return file => !file || file.size <= size;
}
