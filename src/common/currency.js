function getCurrencyOptions(currency) {
  const config = {
    RUB: {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    },
  };
  return Object.assign({
    style: 'currency',
    currency,
  }, config[currency] || {});
}

export function formatCurrency(number, currency, locale) {
  const options = getCurrencyOptions(currency);
  return new Intl.NumberFormat(locale, options).format(number);
}
