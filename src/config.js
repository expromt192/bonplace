export default {
  defaultLocale: 'ru-RU',
  api: `${process.env.VUE_APP_API_URL}/v1`,
  recaptchaKey: '6Leex-YUAAAAADEhfKmlJHPd3xEJ-CncbReBUSaH',
};
