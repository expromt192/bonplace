import Vue from 'vue';
import VueMeta from 'vue-meta';
import { FormFilePlugin, PaginationNavPlugin, TooltipPlugin, ToastPlugin, ModalPlugin, ImagePlugin, VBScrollspyPlugin, CollapsePlugin, ListGroupPlugin, NavPlugin } from 'bootstrap-vue';
import Vuelidate from 'vuelidate';
import axios from 'axios';
import join from 'url-join';
import qs from 'qs';
import config from './config';

import './scss/style.scss';

import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';

const token = localStorage.getItem('access-token');
if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

const isAbsoluteURLRegex = /^(?:\w+:)\/\//;
axios.interceptors.request.use((axiosRequestConfig) => {
  if (!isAbsoluteURLRegex.test(axiosRequestConfig.url)) {
    axiosRequestConfig.url = join(config.api, axiosRequestConfig.url);
  }
  return axiosRequestConfig;
});
axios.interceptors.request.use((config) => {
  config.paramsSerializer = params => qs.stringify(params);
  return config;
});
axios.interceptors.response.use(null, (error) => {
  if (error.response.status === 401) {
    // Выход если пользователь заблокирован
    store.dispatch('logout');
    router.push({ path: '/' });
  }
  return error;
});


Vue.config.productionTip = false;
Vue.use(VueMeta);
Vue.use(Vuelidate);

// bootstrap-vue
Vue.use(FormFilePlugin);
Vue.use(PaginationNavPlugin);
Vue.use(TooltipPlugin);
Vue.use(ToastPlugin);
Vue.use(ModalPlugin);
Vue.use(ImagePlugin);
Vue.use(VBScrollspyPlugin);
Vue.use(CollapsePlugin);
Vue.use(ListGroupPlugin);
Vue.use(NavPlugin);
store.$http = axios;
Vue.prototype.$http = axios;
Vue.prototype.$config = config;

new Vue({
  i18n,
  router,
  store,
  render: h => h(App),
  data: {
    // lastScrollPosition: 0,
  },
  methods: {
    onScroll() {
      const currentScrollPosition = window.pageYOffset || document.documentElement.scrollTop;
      if (currentScrollPosition < 70) {
        this.$store.commit('unstickHeader');
        return;
      }
      // Stop executing this function if the difference between
      // current scroll position and last scroll position is less than some offset
      // if (Math.abs(currentScrollPosition - this.lastScrollPosition) < 60) {
      //   return;
      // }
      // if (currentScrollPosition < this.lastScrollPosition) {
      //   this.$store.commit('stickHeader');
      // } else {
      //   this.$store.commit('unstickHeader');
      // }
      // this.lastScrollPosition = currentScrollPosition;
      this.$store.commit('stickHeader');
    },
  },
  mounted() {
    window.addEventListener('scroll', this.onScroll);
    this.$store.dispatch('fetchConfig');
  },
  beforeDestroy() {
    window.removeEventListener('scroll', this.onScroll);
  },
}).$mount('#app');
