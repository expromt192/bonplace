import { getApiUrl } from '../../common/api';

const state = {
  token: localStorage.getItem('access-token') || '',
};

const getters = {
  isAuthenticated: state => !!state.token,
};

const actions = {
  async login({ commit, dispatch }, user) {
    const response = await this.$http.post(getApiUrl('login'), user);
    if (response.data.success) {
      commit('authorize', response.data.access_token);
      // dispatch('fetchUser');
    }
    return response.data;
  },
  async logout({ commit }) {
    commit('unauthorize');
  },
  async loginAccessToken({ commit, dispatch }, accessToken) {
    commit('authorize', accessToken);
    // dispatch('fetchUser');
  },
  async loginByPass({ commit, dispatch }, token) {
    const response = await this.$http.post(getApiUrl('loginByPass'), { token });
    if (response.data.success) {
      commit('authorize', response.data.access_token);
      // dispatch('fetchUser');
    }
    return response.data;
  },
  async passwordReset({ state }, payload) {
    const response = await this.$http.post(getApiUrl('passwordReset'), payload);
    return response.data;
  },
  async setNewPassword({ state }, payload) {
    const response = await this.$http.post(getApiUrl('setNewPassword'), payload);
    return response.data;
  },
};

const mutations = {
  authorize(state, token) {
    localStorage.setItem('access-token', token);
    state.token = token;
    this.$http.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  },
  unauthorize(state) {
    localStorage.removeItem('access-token');
    state.token = '';
    delete this.$http.defaults.headers.common['Authorization'];
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
