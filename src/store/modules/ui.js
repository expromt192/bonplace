const state = {
  sidebar: false,
  sticky: false,
  toasts: [],
};

const getters = {
  toasts: state => state.toasts,
};

const actions = {
  showToast({ commit }, { content, variant = 'default', icon = null, timeout = 5000 }) {
    const toast = {
      content,
      icon,
      variant,
      id: Date.now(),
    };
    commit('showToast', toast);
    setTimeout(() => commit('hideToast', toast), timeout);
  },
  hideToast({ commit, state }, index) {
    commit('hideToast', state.toasts[index]);
  },
};

const mutations = {
  showSidebar(state) {
    state.sidebar = true;
    this.commit('blockScroll');
  },
  hideSidebar(state) {
    state.sidebar = false;
    this.commit('unblockScroll');
  },
  blockScroll() {
    document.body.className += ' block-scroll';
  },
  unblockScroll() {
    document.body.className = document.body.className.replace(' block-scroll', '');
  },
  stickHeader(state) {
    state.sticky = true;
  },
  unstickHeader(state) {
    state.sticky = false;
  },
  showToast(state, toast) {
    state.toasts.push(toast);
  },
  hideToast(state, toast) {
    const index = state.toasts.indexOf(toast);
    if (index !== -1) {
      state.toasts.splice(index, 1);
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
