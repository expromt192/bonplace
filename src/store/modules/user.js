import { getApiUrl } from '../../common/api';
import config from '../../config';

const state = {
  user: {},
};

const getters = {
  user: state => state.user,
  locale: state => (state.user && state.user.profile && state.user.profile.locale) || config.defaultLocale,
};

const actions = {
  async fetchUser({ state, dispatch }) {
    const response = await this.$http.get(getApiUrl('user'));
    state.user = response.data ? response.data : {};
  },
  async updateProfile({ dispatch }, payload) {
    const response = await this.$http.put(getApiUrl('profile'), payload);
    dispatch('fetchUser');
    return response.data;
  },
};

const mutations = {};

export default {
  state,
  getters,
  actions,
  mutations,
};
