import { getApiUrl } from '../../common/api';
import config from '../../config';

const state = {
  config: {},
};

const getters = {
  config: state => state.config,
};

const actions = {
  async fetchConfig({ commit }) {
    const response = await this.$http.get(getApiUrl('config'));
    const data = response.data ? response.data : {};
    commit('setConfig', data);
  },
};

const mutations = {
  setConfig(state, data) {
    state.config = {};
    data.forEach((d) => {
      state.config[d.key] = d.value;
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
